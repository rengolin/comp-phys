/* Gnuplot helper */
#include <cassert>
#include <vector>
#include <string>
#include <fstream>

namespace Gnuplot {
  // Basic plot structure, with tag for legend
  struct Plot {
    std::string Tag;
    std::vector<double> X;
    std::vector<double> Y;
    void clear() {
      Tag.clear();
      X.clear();
      Y.clear();
    }
  };

  // Standard type for passing results
  typedef std::vector<std::string> tag_t;
  typedef std::vector<Plot> plot_t;

  // Gnuplot driver, prints out plot Files
  // Requires Gnuplot 5.x variable support
  class Driver {
    // Basic properties
    std::fstream File;
    std::string Name;
    const std::string Ext;

    // Plot parameters
    std::pair<double, double> RangeX;
    std::pair<double, double> RangeY;
    // Series parameters
    tag_t TagNames; // Legend
    size_t Idx; // DataN index in ranges

  public:
    Driver(std::string Name) :
           Name(Name), Ext(".gp"), RangeX({0.0, 0.0}), RangeY({0.0, 0.0}), Idx(0) {
      // Make sure filename ends with .gp
      if (!std::equal(Name.rbegin(), Name.rend(), Ext.rbegin()))
        Name += Ext;
      File.open(Name, std::ios_base::out);
      assert(File.is_open());

      // Gnuplot header
      // TODO: Differentiate types of graphs, terminal, etc.
      File << "# Gnuplot file: " << Name << std::endl;
      File << "set terminal x11 background rgb 'white'" << std::endl;
      File << "set title '" << Name << "' font ',20'" << std::endl;
      File << "set key left box" << std::endl;
    }

    // Create variables with data series, name then locally
    void CollectSeries(const plot_t &Plots, int RangeIdx=-1) {
      // Name range with tag, print one range per tag
      size_t CurIdx = 0;
      for (auto Plot: Plots) {
        // Cache (for final print, labels)
        TagNames.push_back(Plot.Tag);

        // Create data range as a variable named after the tag
        File << "$data" << Idx++ << " << EOD" << std::endl;

        // Print the whole range (limit on X)
        assert(Plot.X.size() <= Plot.Y.size());
        for (size_t i=0, LastIdx=Plot.X.size(); i<LastIdx; i++) {
          File << Plot.X[i] << " " << Plot.Y[i] << std::endl;

          // Update range with the right range (or all, if none chosen)
          if (RangeIdx >= 0 && RangeIdx != CurIdx)
            continue;
          RangeX.first = std::min(RangeX.first, Plot.X[i]);
          RangeX.second = std::max(RangeX.second, Plot.X[i]);
          RangeY.first = std::min(RangeY.first, Plot.Y[i]);
          RangeY.second = std::max(RangeY.second, Plot.Y[i]);
        }

        // Close range
        File << "EOD" << std::endl;
        CurIdx++;
      }
    }

    // Use local names to refer to data series, close File
    void Print() {
      File << "set xrange [" << RangeX.first << ":" << (RangeX.second*1.1) << "]" << std::endl;
      File << "set yrange [" << RangeY.first << ":" << (RangeY.second*1.1) << "]" << std::endl;
      File << "plot ";
      size_t i = 0;
      for (auto Tag: TagNames)
        File << "'$data" << i++ << "' using 1:2 title '" << Tag << "' with lines, ";
      assert(i == Idx);
      File << std::endl;
      File << "pause mouse key" << std::endl;
      File.close();
    }
  };

} // namespace Gnuplot
