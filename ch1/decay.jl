# Constants for the example
l2 = log(2)
runs = [ "Ruthefordium 261: ~81 seconds half-life",
         "Caesium 137: ~30 years half-life (952e6 seconds)",
         "Carbon 14: ~5730 years half-life (181e9 seconds)" ]
hls = [ 81, 956e6, 181e9 ]

# Decay function itself
function decay(Tau, i, d, t)
    dt = d/Tau
    td = t/d
    res = [0 i]
    for j = 1:td
        n0 = res[end][end]
        n  = n0 - (n0 * dt)
        res = vcat(res, [d*j n])
    end
    return res
end

# Plot init
print("Initialising Plotly (takes a while)...\n")
using Plots
plotly()

# Main loop: generate, plot, repeat
for i = 1:length(runs)
    title=runs[i]
    print("Running $title...\n")
    hl = hls[i]
    max = hl*5
    tau = hl/l2
    ex1 = decay(tau, 100, hl/100, max)
    ex2 = decay(tau, 100, hl/20, max)
    ex3 = decay(tau, 100, hl/4, max)

    print("Preparing plots...\n")
    pex =  plot(ex1[1:end, 1], ex1[1:end, 2],
                title=title,
                label="half-life / 100")
    pex = plot!(ex2[1:end, 1], ex2[1:end, 2],
                label="half-life / 20")
    pex = plot!(ex3[1:end, 1], ex3[1:end, 2],
                label="half-life / 4")

    print("Displaying plots...\n")
    display(pex)
end

