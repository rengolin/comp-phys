# Julia can spend considerable time in installing and building
# packages, so this is a wrapper to install all necessary ones
# for the programs in this repository.

# Due to package management differences, you may have to install
# some packages by hand using your distro pkgmgr.

using Pkg
Pkg.add("Plots")
using Plots
plotly()

# PyPlot needs Tk installed and enabled in Python
Pkg.add("PyPlot")
Pkg.add("PyCall")
Pkg.add("LaTeXStrings")
using PyPlot
pyplot()
