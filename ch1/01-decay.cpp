/* Section 1.1 - Radioactive Decay
 *
 * dNr/dt = - Nr/T
 *
 * where:
 *   Nr = number of radioactive elements
 *    T = Tau, "time constant" (mean-life of a nucleus)
 *
 * Definitions:
 *   Tau [mean life]: each atom's life expectancy
 *   T(1/2) [half life]: time for half atoms to decay
 *   Tau = T(1/2) / ln(2)
 *
 * ** Euler Method:
 *
 * First order, so: Nr = Nr(0)e^(-t/T)
 * is a solution.
 *
 * Taylor expansion:
 *  1. Nr(Dt) = Nr(0) + [ dNr/dt . Dt ] + [ 1/2 . d^2Nr/dt^2 . Dt^2 ] + ...
 *
 * But Dt (delta t) tends to zero, so, only take first (non-const) term.
 * With error in the order of Dt^2.
 *
 * Derivating Nr:
 *  2. dNr/dt ~ [ Nr(t+Dt) - Nr(t) ] / Dt ---> Nr(t+Dt) = Nr(t) + dNr/dt . Dt
 *
 * Step function, then, is:
 *  3. Nr(t+Dt) = Nr(t) - Nr(t)/T . Dt [ 1. + 2. ]
 *
 * Error:
 *   E(step) ~ Dt^2 [ from 1. ]
 *   E(global) ~ (num steps) . Dt^2 ~ t/Dt . Dt^2 ~ tDt
 *
 */
#include <cstdint>
#include <cmath>
#include <vector>
#include <fstream>
#include "../common/gnuplot.h"

// Calculate decay incrementally from t=0 to Time
// TODO: estimate best time&dt based on mean-life
class Decay {
  const double Tau;
  Gnuplot::plot_t Results;

  void Clear(std::string &Tag, double Initial) {
    // Cache first (and only) elenemt
    auto Result = &Results[0];

    // Initialise (allow for multiple runs)
    Result->clear();
    Result->Tag = Tag;
    Result->X.push_back(0.0);
    Result->Y.push_back(Initial);
  }

public:
  Decay(double Tau) : Tau(Tau), Results(1) { }

  // Return const reference, we don't expect results to outlve call
  // FIXME: Could use move semantics here with unique_ptr instead of const ref
  const Gnuplot::plot_t &Run(std::string Tag,
                             double Initial,
                             double Delta,
                             double Time) {
    // Initialise (allow for multiple runs)
    Clear(Tag, Initial);

    // Cache first (and only) elenemt
    auto Result = &Results[0];

    // Cache global parameters
    double DtTau = Delta/Tau;

    // Iterate (FIXME: Total can overflow)
    for (size_t i=1, Total = Time / Delta; i<Total; i++) {
      double Nr0 = Result->Y.back();

      // Next from Euler expansion [Nr - (Nr.Dt)]]
      double Nr = Nr0 - (Nr0 * DtTau);

      // Append current point to results
      Result->X.push_back(Delta*i);
      Result->Y.push_back(Nr);
    }

    // Return results
    return Results;
  }
};

// Hard-coded examples, test for exact output
int main() {
  // Chapter example
  Gnuplot::Driver gp1("Chapter1-Tau1-Nr100-Dt.5.2.05");
  Decay ch1(1);
  gp1.CollectSeries(ch1.Run("Dt = 0.5s", 100, 0.5, 5));
  gp1.CollectSeries(ch1.Run("Dt = 0.2s", 100, 0.2, 5));
  gp1.CollectSeries(ch1.Run("Dt = 0.05s", 100, 0.05, 5));
  // Print results to gnuplot file
  gp1.Print();

  // Some constants
  const double day = 24 * 3600;
  const double month = day * 30.43685; // average month on an exact year
  const double year = day * 365.2422; // exact year

  // Some known elements

  // Ruthefordium 261: ~81 seconds half-life
  const double hlr261 = 81;
  const double maxr261 = hlr261 * 5;
  Gnuplot::Driver gr261("Ruthefordium-HL81s-Nr400s");
  Decay r261(hlr261 / log(2));
  gr261.CollectSeries(r261.Run("Dt = 1s", 100, 1, maxr261));
  gr261.CollectSeries(r261.Run("Dt = 10s", 100, 10, maxr261));
  gr261.CollectSeries(r261.Run("Dt = 20s", 100, 20, maxr261));
  gr261.Print();

  // Caesium 137: ~30 years half-life (952e6 seconds)
  const double hlc137 = 956e6;
  const double maxc137 = hlc137 * 5;
  Gnuplot::Driver gc137("Caesium137-HL30y-Nr150y");
  Decay c137(hlc137 / log(2));
  gc137.CollectSeries(c137.Run("Dt = 1y", 100, year, maxc137));
  gc137.CollectSeries(c137.Run("Dt = 10y", 100, 10*year, maxc137));
  gc137.CollectSeries(c137.Run("Dt = 20y", 100, 20*year, maxc137));
  gc137.Print();

  // Carbon 14: ~5730 years half-life (181e9 seconds)
  const double hlc14 = 181e9;
  const double maxc14 = hlc14 * 5;
  Gnuplot::Driver gc14("Carbon14-HL5730y-Nr25000y");
  Decay c14(hlc14 / log(2));
  gc14.CollectSeries(c14.Run("Dt = 100y", 100, 100*year, maxc14));
  gc14.CollectSeries(c14.Run("Dt = 1000y", 100, 1000*year, maxc14));
  gc14.CollectSeries(c14.Run("Dt = 3500y", 100, 3500*year, maxc14));
  gc14.Print();

  return 0;
}
