/*
 * Chapter 2: Projectile Motion
 *
 * Cannon shell: parabolic trajectory
 *   a(x) = d2x/dx2 = - drag
 *   a(y) = d2y/dy2 = - g - drag
 *   Second derivative is not good with Euler, so we split:
 *
 *   dx/dt = vx, dv/dt = - drag
 *   dy/dt = vy, dv/dt = -g -drag
 *
 * Drag:
 *   F = -(1/2)C(ro)Av^2
 *   Fx = F.cos(teta) = F.(vx/v)
 *   Fy = F.sin(teta) = F.(vy/v)
 */

#include <vector>
#include <string>
#include <fstream>
#include <cmath>
#include "../common/gnuplot.h"

// Calculates velocities and position for cannon shell by
// separating dx and dy to make it easier on the Euler method.
class CannonShell {
  double Vel;
  double Angle;
  Gnuplot::plot_t Results;

  void Clear() {
    auto Pos = &Results[0];
    Pos->clear();
    Pos->Tag = "Position";
    Pos->X.push_back(0.0);
    Pos->Y.push_back(0.0);
  }

public:
  CannonShell(double Vel, double Angle) : Vel(Vel), Angle(Angle), Results(1) { }

  // Return const reference, we don't expect results to outlve call
  // FIXME: Could use move semantics here with unique_ptr instead of const ref
  const Gnuplot::plot_t &Run(double Initial, double Delta, double Time) {
    // Initialise (allow for multiple runs)
    Clear();

    // Starting positions
    auto PosRes = &Results[0];
    double PosX = PosRes->X.back();
    double PosY = PosRes->Y.back();

    // Evolution parameters (not on plot)
    double VelX = Vel*cos(Angle), VelY = Vel*sin(Angle);
    double G = -9.8;
    double B2m = 4e-5; // See chapter 5, Bennet 1976
    double M = 1; // 1 kg

    // Iterate (FIXME: Total can overflow)
    for (size_t i=1, Total = Time / Delta; i<Total; i++) {
      // Absolute velocity
      double V = sqrt(VelX*VelX + VelY*VelY);
      // Drag
      double DragX = -B2m*V*VelX;
      double DragY = -B2m*V*VelY;
      // Velocity
      VelX += (DragX/M)*Delta;
      VelY += (G+DragY/M)*Delta;
      // Position is S(Vdt)
      PosX += VelX*Delta;
      PosY += VelY*Delta;

      // Exit wen the shell hits the ground
      if (PosY < 0)
          break;

      // Append current point to results
      PosRes->X.push_back(PosX);
      PosRes->Y.push_back(PosY);
    }

    // Return results
    return Results;
  }
};

// Hard-coded examples, test for exact output
int main() {
  // Variations
  Gnuplot::Driver ga("CannonShell");
  CannonShell a(700, 45);
  ga.CollectSeries(a.Run(0, 0.1, 200));
  ga.Print();

  return 0;
}
