/*
 * Chapter 2: Projectile Motion
 *
 * Bicycle: mass m, on a flat road
 *   E = (mv^2)/2 (only kinectic)
 *   P = dE/dv = mv(dv/dt)
 *   dv/dt = P/mv
 *
 * With:
 *   P = cyclist's power (up to 400W for an hour)
 *
 * Using Euler approximation:
 *   dv/dt = (v - v0)/Dt = P/mv
 *   v = v0 + (P/mv).Dt
 *
 * Adding drag:
 *   F ~ -(1/2) C (ro) A v^2
 *
 * With:
 *   C = drag coefficient (depends on shape)
 *   ro = air density (1.1644 at 300K)
 *   A = frontal area of the object
 *
 * Final Euler approximation:
 *   v = v0 + (P/mv).Dt - (1/2m)C(ro)Av^2.Dt
 *
 * Constant sources:
 *   https://en.wikipedia.org/wiki/Drag_coefficient
 *   https://en.wikipedia.org/wiki/Density_of_air
 */

#include <vector>
#include <string>
#include <fstream>
#include "../common/gnuplot.h"

// Most mass & drag from bicycles are usually negligible
struct Bicycle {
  double Mass;
  double FrontalArea; // Which contributes to drag
  double DragCoef;    // Shape/Material based drag
};

// Most of the mass & drag is in the cyclist 
struct Cyclist {
  double Mass;
  double Power;       // Energy on the pedals
  double FrontalArea; // Which contributes to drag
  double DragCoef;    // Shape/Material based drag (helmet, clothes)
};

// Calculates velocities and position for bicycles and cyclists.
class Cycle {
  Bicycle B;
  Cyclist C;
  double ro;
  Gnuplot::plot_t Results;

  void Clear() {
    // Cache elements
    auto Pos = &Results[0];
    auto Vel = &Results[1];
    auto Accel = &Results[2];

    Pos->clear();
    Pos->Tag = "Position";
    Pos->X.push_back(0.0);
    Pos->Y.push_back(0.0);

    // Initial velocity can't be zero (divides in Push equation)
    Vel->clear();
    Vel->Tag = "Velocity";
    Vel->X.push_back(0.0);
    Vel->Y.push_back(1.0);

    Accel->clear();
    Accel->Tag = "Acceleration";
    Accel->X.push_back(0.0);
    Accel->Y.push_back(0.0);
  }

public:
  Cycle(Bicycle B, Cyclist C, double ro) : B(B), C(C), ro(ro), Results(3) { }

  // Return const reference, we don't expect results to outlve call
  // FIXME: Could use move semantics here with unique_ptr instead of const ref
  const Gnuplot::plot_t &Run(double Initial, double Delta, double Time) {
    // Initialise (allow for multiple runs)
    Clear();

    // Cache elements
    auto PosRes = &Results[0];
    auto VelRes = &Results[1];
    auto AccelRes = &Results[2];

    // Cache global parameters
    double M = B.Mass + C.Mass;
    double D = B.DragCoef + C.DragCoef;
    double A = B.FrontalArea + C.FrontalArea;

    // Iterate (FIXME: Total can overflow)
    for (size_t i=1, Total = Time / Delta; i<Total; i++) {
      double P0 = PosRes->Y.back();
      double V0 = VelRes->Y.back();

      // Acceleration is dV/dt = P/mv
      double Accel = (C.Power)/(M*V0);
      // Velocity from Euler expansion [P.Dt/mv]
      double Push = Accel*Delta;
      double Drag = D*ro*A*(V0*V0)*Delta/(2*M);
      double Vel = V0 + Push - Drag;
      // Position is S(Vdt)
      double Pos = P0 + Vel*Delta;

      // Append current point to results
      PosRes->X.push_back(Delta*i);
      PosRes->Y.push_back(Pos);
      VelRes->X.push_back(Delta*i);
      VelRes->Y.push_back(Vel);
      AccelRes->X.push_back(Delta*i);
      AccelRes->Y.push_back(Accel);
    }

    // Return results
    return Results;
  }
};

// Hard-coded examples, test for exact output
int main() {
  // See sources for constants above
  double ro = 1.1644;

  // Bicycle { mass, area, drag }
  Bicycle SmallB({5, 0.05, 0.1}), BigB({10, 0.11, 0.2});
  // Cyclist { mass, power, area, drag }
  Cyclist SmallC({50, 50, 0.15, 0.3}), BigC({70, 400, 0.22, 0.4});

  // Variations
  Gnuplot::Driver gss("SmallBicycle-SmallCyclist");
  Cycle ss(SmallB, SmallC, ro);
  gss.CollectSeries(ss.Run(0, 0.1, 200), 1);
  gss.Print();

  Gnuplot::Driver gsb("SmallBicycle-BigCyclist");
  Cycle sb(SmallB, BigC, ro);
  gsb.CollectSeries(sb.Run(0, 0.1, 200), 1);
  gsb.Print();

  Gnuplot::Driver gbs("BigBicycle-SmallCyclist");
  Cycle bs(BigB, SmallC, ro);
  gbs.CollectSeries(bs.Run(0, 0.1, 200), 1);
  gbs.Print();

  Gnuplot::Driver gbb("BigBicycle-BigCyclist");
  Cycle bb(BigB, BigC, ro);
  gbb.CollectSeries(bb.Run(0, 0.1, 200), 1);
  gbb.Print();

  return 0;
}
