# Computational Physiscs

Implementation of the code in the book "Compuational Physics, 2nd Ed., Giordano&Nakanishi, Pearson".

http://www.physics.purdue.edu/~hisao/book/

This is not a copy of the code available in the book, but my personal take on it. You can find the sample programs on the URL above.

I may try to improve code reusability and extensibility, make use of more powerful methods, try to help the compiler optimise it, etc.

## Building

Every file is meant as a different unit, so simply calling the compiler on the source file should work.

Include paths are relative (../common) to make sure you're in the directory already.

If the file needs special flags, or the point of it is to try different flags, it'll be explicit on the head comment.